import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class Timers extends JFrame{	
	private BufferedImage homeI = ImageIO.read(new File("home.png"));
	private BufferedImage playI = ImageIO.read(new File("play.png"));
	private BufferedImage pauseI = ImageIO.read(new File("pause.png"));
	private BufferedImage stopI = ImageIO.read(new File("stop.png"));	
	private JLabel timer = new JLabel();
	private JPanel stopWatch = new JPanel();	
	private JButton start = new JButton(new ImageIcon(playI));
	private JButton pause = new JButton(new ImageIcon(pauseI));
	private JButton clear = new JButton(new ImageIcon(stopI));
	private JPanel buttonPanel = new JPanel();	
	private JButton homeButton = new JButton(new ImageIcon(homeI));
	private JPanel home = new JPanel();	
	private Timer javaTimer = new Timer(1000,new TimerListener());
	private int count;
	private BufferedImage one = ImageIO.read(new File("1.png"));
	private BufferedImage two = ImageIO.read(new File("2.png"));
	private BufferedImage three = ImageIO.read(new File("3.png"));
	private BufferedImage four = ImageIO.read(new File("4.png"));
	private BufferedImage five = ImageIO.read(new File("5.png"));
	private BufferedImage six = ImageIO.read(new File("6.png"));
	private BufferedImage seven = ImageIO.read(new File("7.png"));
	private BufferedImage eight = ImageIO.read(new File("8.png"));
	private BufferedImage nine = ImageIO.read(new File("9.png"));
	private BufferedImage clear1 = ImageIO.read(new File("clear.png"));
	private BufferedImage zero = ImageIO.read(new File("0.png"));
	private BufferedImage enter = ImageIO.read(new File("enter.png"));
	private BufferedImage[] buttonImages = { zero, one, two, three, four, five,
			six, seven, eight, nine };		
	private JButton[] numberButtons = new JButton[10];
	private JButton enterButton, clearButton, zeroButton;	
	private JPanel displayPanel = new JPanel(),	pinpadPanel = new JPanel(new GridLayout(5,3)),top = new JPanel();
	private int input =0;
	private JTextField number = new JTextField();
	
	public Timers() throws IOException {			
		for (int i = 1; i <= 9; i++) {
			numberButtons[i] = new JButton(new ImageIcon(buttonImages[i]));
			final int x = i;
			numberButtons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					input=(input*10)+x;
					String newInput=input+" second Timer.";
					number.setText(newInput);
					number.setFont(new Font("MS San Serif", Font.BOLD, 24));
				}
			});
			pinpadPanel.add(numberButtons[i]);
		}
		clearZeroEnterButtons();		
		count = input;
		addButtonFunction();
		timer.setFont(new Font("MS San Serif",Font.BOLD,30));
		stopWatch.add(timer);
		top.add(stopWatch,BorderLayout.CENTER);		
		buttonPanel.setLayout(new GridLayout(1,3));
		buttonPanel.add(start);
		buttonPanel.add(clear);
		top.add(buttonPanel);
		top.add(number);		
		add(top);
		pinpadPanel.add(homeButton);
		add(pinpadPanel);
		
		setLayout(new GridLayout(2,1));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(300,470);
		setTitle("IOS Demo");
	}
	private void addButtonFunction(){	
		start.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent e){	
				count=input;
				javaTimer.start();}}
		);
		clear.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				javaTimer.stop();
				count=input;
				timer.setText(count+" seconds left.");}});		
		homeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){	
				javaTimer.stop();
				dispose();
				try {Phone menu = new Phone();} 
				catch (IOException e1) {e1.printStackTrace();}}});
	}
	public class TimerListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {			
			if (count == 0) 
			{
			    javaTimer.stop();
			} 
			else 
			{
			    count--;
			    timer.setText("" + count+" seconds left.");
			}
		}
	}	
	private void clearZeroEnterButtons() {
		clearButton = new JButton(new ImageIcon(clear1));
		clearButton.addActionListener(new ClearListener());			
		zeroButton = new JButton(new ImageIcon(zero));
		zeroButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input=(input*10)+0;
				String newInput=input+" second Timer.";
				number.setText(newInput);
				number.setFont(new Font("MS San Serif", Font.BOLD, 24));}});
		pinpadPanel.add(clearButton);
		pinpadPanel.add(zeroButton);
	}	
	class ClearListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			number.setText("");
			input=0;
		}
	}	
}
