import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JFileChooser;

public class Settings extends JFrame implements ChangeListener
{
	private static JFileChooser chooser=new JFileChooser();
	private JPanel sliderPanel = new JPanel();
	int colorRed=0, colorGreen=0, colorBlue=0;
	
	private JLabel brightnesslbl = new JLabel("Brightness");
	private JLabel volumelbl = new JLabel("    Volume    ");
	private JLabel volumePercent = new JLabel();
	private JSlider brightnessSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 255);
	private JSlider volumeSlider = new JSlider(JSlider.HORIZONTAL, 0, 100,50);
	
	private BufferedImage homeI = ImageIO.read(new File("home.png"));
	private JButton homeButton = new JButton(new ImageIcon(homeI));
	private JButton backGround = new JButton("Change Background");
	
	
	
	public Settings() throws IOException
	{		
		sliderPanel.setBorder(new CompoundBorder(new TitledBorder("Settings"),new EmptyBorder(2, 2, 2, 2)));		
		brightnessSlider.addChangeListener(this);
		volumeSlider.addChangeListener(this);
		
		sliderPanel.add(brightnesslbl);
		sliderPanel.add(brightnessSlider);
		sliderPanel.add(volumelbl);
		sliderPanel.add(volumeSlider);
		sliderPanel.add(volumePercent);
		backGround.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){				
				String fileName=System.getProperty("user.dir");
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal==JFileChooser.APPROVE_OPTION) {
					fileName=chooser.getSelectedFile().getName();
					System.out.println("You choose to open this file: "+chooser.getSelectedFile().getName());
				}}});
		sliderPanel.add(backGround);
		add(sliderPanel);		
		
		homeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){	
				dispose();
				try {Phone menu = new Phone();} 
				catch (IOException e1) {e1.printStackTrace();}}});
		add(homeButton,BorderLayout.SOUTH);		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		setSize(300,470);
		setTitle("IOS Demo");	
	}
	@Override
	public void stateChanged(ChangeEvent e) 
	{
		JSlider slider = (JSlider) e.getSource();
		
		if (slider == brightnessSlider) {
			colorRed = (int)slider.getValue();
			colorGreen = (int)slider.getValue();
			colorBlue = (int)slider.getValue();
			Color colTest = new Color(colorRed,colorGreen,colorBlue);
			sliderPanel.setBackground(colTest);
		} else {
			String input =(int)slider.getValue()+"";
			volumePercent.setText("        "+input+"%        ");			
		}		
	}	
}
