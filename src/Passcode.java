import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

public class Passcode extends JFrame {
	private JPanel 
			displayPanel = new JPanel(),
			pinpadPanel = new JPanel(),
			enterPasscodePanel = new JPanel();
	private JButton[] numberButtons = new JButton[10];
	private JButton enterButton, clearButton, zeroButton;
	private JPasswordField passwordField = new JPasswordField();
	private JLabel enterPasscode = new JLabel("ENTER PASSCODE");
	private String input = "";
	private int check = 0;
	private static String pin = "1313";	
	private BufferedImage one = ImageIO.read(new File("1.png"));      
	private BufferedImage two = ImageIO.read(new File("2.png"));  
	private BufferedImage three = ImageIO.read(new File("3.png"));
	private BufferedImage four = ImageIO.read(new File("4.png"));     
	private BufferedImage five = ImageIO.read(new File("5.png")); 
	private BufferedImage six = ImageIO.read(new File("6.png"));
	private BufferedImage seven = ImageIO.read(new File("7.png"));    
	private BufferedImage eight = ImageIO.read(new File("8.png"));
	private BufferedImage nine = ImageIO.read(new File("9.png"));
	private BufferedImage clear = ImageIO.read(new File("clear.png"));
	private BufferedImage zero = ImageIO.read(new File("0.png")); 
	private BufferedImage enter = ImageIO.read(new File("enter.png"));	
	private BufferedImage[] buttonImages= {zero,one,two,three,four,five,six,seven,eight,nine};

	public Passcode() throws IOException {
		passwordField.setFont(new Font("MS San Serif",Font.BOLD,17));
		
		displayPanel.setLayout(new BorderLayout());
		displayPanel.add(passwordField,BorderLayout.NORTH);
		
		pinpadPanel.setLayout(new GridLayout(4,3));		
		for(int i=1;i<=9;i++) {	
			numberButtons[i] = new JButton(new ImageIcon(buttonImages[i]));
			numberButtons[i].setMargin(new Insets(0,0,0,0));
			numberButtons[i].setContentAreaFilled(false);
			numberButtons[i].setBorder(new EmptyBorder(0,0,0,0));
			numberButtons[i].setFocusPainted(false);
			final int x = i;
			numberButtons[i].addActionListener(new ActionListener()	{
				public void actionPerformed(ActionEvent e) {	
					input+=x;
					passwordField.setText(input);}});
			pinpadPanel.add(numberButtons[i]);
		}		
		clearZeroEnterButtons();
		displayPanel.add(pinpadPanel,BorderLayout.CENTER);			
		enterPasscodePanel.add(enterPasscode);
		enterPasscodePanel.setSize(320,20);		
		add(enterPasscodePanel);
		add(displayPanel);
		setVisible(true);
		setSize(300,460);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("IOS Demo");
	}	
	private void clearZeroEnterButtons() {
		clearButton = new JButton(new ImageIcon(clear));
		clearButton.addActionListener(new ClearListener());	
		clearButton.setMargin(new Insets(0,0,0,0));
		clearButton.setContentAreaFilled(false);
		clearButton.setBorder(new EmptyBorder(0,0,0,0));
		clearButton.setFocusPainted(false);
		final int z = 0;
		zeroButton = new JButton(new ImageIcon(zero));
		zeroButton.setMargin(new Insets(0,0,0,0));
		zeroButton.setContentAreaFilled(false);
		zeroButton.setBorder(new EmptyBorder(0,0,0,0));
		zeroButton.setFocusPainted(false);
		zeroButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+=z;
				passwordField.setText(input);}});		
		enterButton = new JButton(new ImageIcon(enter));
		enterButton.setMargin(new Insets(0,0,0,0));
		enterButton.setContentAreaFilled(false);
		enterButton.setBorder(new EmptyBorder(0,0,0,0));
		enterButton.setFocusPainted(false);
		enterButton.addActionListener(new EnterListener());		
		pinpadPanel.add(clearButton);
		pinpadPanel.add(zeroButton);
		pinpadPanel.add(enterButton);
	}	
	class ClearListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			passwordField.setText("");
			input="";
		}
	}	
	class EnterListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {	
			if (authentificationPin(input))	{	
				check=0;
				dispose();				
				try {Phone menu = new Phone();} 
				catch (IOException e1) {e1.printStackTrace();}
			} else {
				check++;
				JOptionPane.showMessageDialog(null, "Wrong Passcode.\n"+(4-check)+" attempts left.");
				if(check==4) {	
					JOptionPane.showMessageDialog(null, "Phone locked for 1 hour.");
					System.exit(0);
				}
			}
		}
	}	
	public static boolean authentificationPin(String input) {	
		if (input.equals(pin))
			return true;
		else
			return false;
	}
	public static void main(String[] args) throws IOException {	
		Passcode Test = new Passcode(); 
	}
}

































