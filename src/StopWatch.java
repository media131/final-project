import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class StopWatch extends JFrame{	
	private BufferedImage homeI = ImageIO.read(new File("home.png"));
	private BufferedImage playI = ImageIO.read(new File("play.png"));
	private BufferedImage pauseI = ImageIO.read(new File("pause.png"));
	private BufferedImage stopI = ImageIO.read(new File("stop.png"));	
	private JLabel timer = new JLabel();
	private JPanel stopWatch = new JPanel();	
	private JButton start = new JButton(new ImageIcon(playI));
	private JButton pause = new JButton(new ImageIcon(pauseI));
	private JButton clear = new JButton(new ImageIcon(stopI));
	private JPanel buttonPanel = new JPanel();	
	private JButton homeButton = new JButton(new ImageIcon(homeI));
	private JPanel home = new JPanel();	
	private Timer javaTimer = new Timer(1000,new TimerListener());
	int input =0;
	private String newInput="";	
	public StopWatch() throws IOException{	
		addButtonFunction();
		timer.setFont(new Font("MS San Serif",Font.BOLD,30));
		stopWatch.add(timer);
		add(stopWatch,BorderLayout.CENTER);		
		buttonPanel.setLayout(new GridLayout(1,3));
		buttonPanel.add(start);
		buttonPanel.add(pause);
		buttonPanel.add(clear);
		add(buttonPanel);
		home.add(homeButton);
		add(home);
		setLayout(new GridLayout(3,1));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(300,470);
		setTitle("IOS Demo");
	}
	private void addButtonFunction() {	
		start.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent e){	
				javaTimer.start();}});
		pause.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){	
				javaTimer.stop();}});
		clear.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				javaTimer.stop();
				input = 0;
				newInput = "0";
				timer.setText(newInput+" seconds.");}});		
		homeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){	
				javaTimer.stop();
				dispose();
				try {Phone menu = new Phone();} 
				catch (IOException e1) {e1.printStackTrace();}}});
	}
	public class TimerListener implements ActionListener 	{
		@Override
		public void actionPerformed(ActionEvent e)
		{	input = input + 1;
			newInput = input + "";
			timer.setText(newInput+" seconds.");
		}
	}
}