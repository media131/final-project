import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Mail extends JFrame{
	private BufferedImage key = ImageIO.read(new File("key.png"));
	private JButton key1 = new JButton("1", new ImageIcon(key));
	private JButton key2 = new JButton("2", new ImageIcon(key));
	private JButton key3 = new JButton("3", new ImageIcon(key));
	private JButton key4 = new JButton("4", new ImageIcon(key));
	private JButton key5 = new JButton("5", new ImageIcon(key));
	private JButton key6 = new JButton("6", new ImageIcon(key));
	private JButton key7 = new JButton("7", new ImageIcon(key));
	private JButton key8 = new JButton("8", new ImageIcon(key));
	private JButton key9 = new JButton("9", new ImageIcon(key));
	private JButton key0 = new JButton("0", new ImageIcon(key));
	private JButton keyExMark = new JButton("@", new ImageIcon(key));
	private JButton keyq = new JButton("q", new ImageIcon(key));
	private JButton keyw = new JButton("w", new ImageIcon(key));
	private JButton keye = new JButton("e", new ImageIcon(key));
	private JButton keyr = new JButton("r", new ImageIcon(key));
	private JButton keyt = new JButton("t", new ImageIcon(key));
	private JButton keyy = new JButton("y", new ImageIcon(key));
	private JButton keyu = new JButton("u", new ImageIcon(key));
	private JButton keyi = new JButton("i", new ImageIcon(key));
	private JButton keyo = new JButton("o", new ImageIcon(key));
	private JButton keyp = new JButton("p", new ImageIcon(key));
	private JButton keyQuestMark = new JButton("?", new ImageIcon(key));
	private JButton keya = new JButton("a", new ImageIcon(key));
	private JButton keys = new JButton("s", new ImageIcon(key));
	private JButton keyd = new JButton("d", new ImageIcon(key));
	private JButton keyf = new JButton("f", new ImageIcon(key));
	private JButton keyg = new JButton("g", new ImageIcon(key));
	private JButton keyh = new JButton("h", new ImageIcon(key));
	private JButton keyj = new JButton("j", new ImageIcon(key));
	private JButton keyk = new JButton("k", new ImageIcon(key));
	private JButton keyl = new JButton("l", new ImageIcon(key));
	private JButton keyApostMark = new JButton("'", new ImageIcon(key));
	private JButton keyEnter = new JButton("\u23CE", new ImageIcon(key));
	private JButton keyz = new JButton("z", new ImageIcon(key));
	private JButton keyx = new JButton("x", new ImageIcon(key));
	private JButton keyc = new JButton("c", new ImageIcon(key));
	private JButton keyv = new JButton("v", new ImageIcon(key));
	private JButton keySpace = new JButton(" ", new ImageIcon(key));
	private JButton keySpace2 = new JButton("C", new ImageIcon(key));
	private JButton keyb = new JButton("b", new ImageIcon(key));
	private JButton keyn = new JButton("n", new ImageIcon(key));
	private JButton keym = new JButton("m", new ImageIcon(key));
	private JButton keyComma = new JButton(",", new ImageIcon(key));
	private JButton keyPeriod = new JButton(".", new ImageIcon(key));
	
	private JButton[] keyboardArray ={			
			key1,key2,key3,key4,key5,key6,key7,key8,key9,key0,keyExMark,
			keyq,keyw,keye,keyr,keyt,keyy,keyu,keyi,keyo,keyp,keyQuestMark,
			keya,keys,keyd,keyf,keyg,keyh,keyj,keyk,keyl,keyApostMark,keyEnter,
			keyz,keyx,keyc,keyv,keySpace,keyb,keyn,keym,keyComma,keyPeriod,keySpace2};
	private JTextField phoneNumber = new JTextField();	
	private String input = "";
	private JLabel enterPhoneNumber = new JLabel("Enter Email Adress");
	private JPanel keyboard = new JPanel();			
	
	public Mail() throws IOException{
		keyboard.setLayout(new GridLayout(4,11));
		createKeyboard();
		addKeyboardFunctions();
				
		phoneNumber.add(enterPhoneNumber);
		enterPhoneNumber.setFont(new Font("MS San Serif", Font.BOLD, 30));
		enterPhoneNumber.setSize(560,40);		
		
		phoneNumber.setFont(new Font("MS San Serif", Font.BOLD, 40));		
		add(phoneNumber);
		add(keyboard,BorderLayout.SOUTH);
		setLayout(new GridLayout(2,1));
		setVisible(true);
		setSize(560,900);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("IOS Demo");
	}	
	private void createKeyboard() {		
		for (int i = 0;i<keyboardArray.length;i++){
			keyboard.add(keyboardArray[i]);
			keyboardArray[i].setHorizontalTextPosition(JButton.CENTER);
			keyboardArray[i].setVerticalTextPosition(JButton.CENTER);
		}
	}
	private void addKeyboardFunctions() {
		
		keyEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Emailing: \n"+phoneNumber.getText());	
				
				
				phoneNumber.setText(input);
				dispose();				
				try {SubjectMail test = new SubjectMail();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});		
		
		key1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="1";
				phoneNumber.setText(input);}});		
		key2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="2";
				phoneNumber.setText(input);}});	
		key3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="3";
				phoneNumber.setText(input);}});	
		key4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="4";
				phoneNumber.setText(input);}});	
		key5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="5";
				phoneNumber.setText(input);}});	
		key6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="6";
				phoneNumber.setText(input);}});	
		key7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="7";
				phoneNumber.setText(input);}});	
		key8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="8";
				phoneNumber.setText(input);}});	
		key9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="9";
				phoneNumber.setText(input);}});	
		key0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="0";
				phoneNumber.setText(input);}});	
		keyExMark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="@";
				phoneNumber.setText(input);}});	
		keyq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="q";
				phoneNumber.setText(input);}});	
		keyw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="w";
				phoneNumber.setText(input);}});	
		keye.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="e";
				phoneNumber.setText(input);}});	
		keyr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="r";
				phoneNumber.setText(input);}});	
		keyt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="t";
				phoneNumber.setText(input);}});	
		keyy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="y";
				phoneNumber.setText(input);}});	
		keyu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="u";
				phoneNumber.setText(input);}});	
		keyi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="i";
				phoneNumber.setText(input);}});	
		keyo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="o";
				phoneNumber.setText(input);}});	
		keyp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="p";
				phoneNumber.setText(input);}});	
		keyQuestMark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="?";
				phoneNumber.setText(input);}});	
		keya.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="a";
				phoneNumber.setText(input);}});	
		keys.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="s";
				phoneNumber.setText(input);}});	
		keyd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="d";
				phoneNumber.setText(input);}});	
		keyf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="f";
				phoneNumber.setText(input);}});	
		keyg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="g";
				phoneNumber.setText(input);}});	
		keyh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="h";
				phoneNumber.setText(input);}});	
		keyj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="j";
				phoneNumber.setText(input);}});	
		keyk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="k";
				phoneNumber.setText(input);}});	
		keyl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="l";
				phoneNumber.setText(input);}});	
		keyApostMark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="'";
				phoneNumber.setText(input);}});			
		keyz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="z";
				phoneNumber.setText(input);}});	
		keyx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="x";
				phoneNumber.setText(input);}});	
		keyc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="c";
				phoneNumber.setText(input);}});	
		keyv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="v";
				phoneNumber.setText(input);}});	
		keySpace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+=" ";
				phoneNumber.setText(input);}});	
		keySpace2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input="";
				phoneNumber.setText(input);}});	
		keyb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="b";
				phoneNumber.setText(input);}});	
		keyn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="n";
				phoneNumber.setText(input);}});	
		keym.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+="m";
				phoneNumber.setText(input);}});	
		keyComma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+=",";
				phoneNumber.setText(input);}});	
		keyPeriod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+=".";
				phoneNumber.setText(input);}});	
	}
}



























