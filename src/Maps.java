import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
public class Maps extends JFrame{	
	private BufferedImage map = ImageIO.read(new File("novamap.png"));
	private JButton mapButton = new JButton(new ImageIcon(map));	
	private BufferedImage homeI = ImageIO.read(new File("home.png"));
	private JButton homeButton = new JButton(new ImageIcon(homeI));
	
	public Maps() throws IOException {
		homeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){	
				dispose();
				try {Phone menu = new Phone();} 
				catch (IOException e1) {e1.printStackTrace();}}});
		
		add(mapButton,BorderLayout.NORTH);
		add(homeButton);
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(300,470);
		setTitle("IOS Demo");
	}
}
