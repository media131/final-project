import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
public class Phone extends JFrame {
	private BufferedImage b2 = ImageIO.read(new File("22.png"));
	private BufferedImage b3 = ImageIO.read(new File("33.png"));
	private BufferedImage b4 = ImageIO.read(new File("44.png"));
	private BufferedImage b5 = ImageIO.read(new File("55.png"));
	private BufferedImage b6 = ImageIO.read(new File("66.png"));
	private BufferedImage messageIcon= ImageIO.read(new File("messages.png"));
	private BufferedImage mailIcon= ImageIO.read(new File("mail.png"));
	private BufferedImage timerIcon= ImageIO.read(new File("timer.png"));
	private BufferedImage stopWatchIcon= ImageIO.read(new File("stopwatch.png"));
	private BufferedImage mapIcon= ImageIO.read(new File("maps.png"));
	private BufferedImage phoneIcon= ImageIO.read(new File("phone.png"));
	private BufferedImage settingsIcon= ImageIO.read(new File("settings.png"));
	private JPanel mainApps = new JPanel();	
	
	private JButton messageButton = new JButton(new ImageIcon(messageIcon));
	private JButton mailButton = new JButton(new ImageIcon(mailIcon));
	private JButton timerButton = new JButton(new ImageIcon(timerIcon));
	private JButton stopWatchButton = new JButton(new ImageIcon(stopWatchIcon));
	private JButton mapButton = new JButton(new ImageIcon(mapIcon));
	private JButton new06 = new JButton(new ImageIcon(b2));
	private JButton new07 = new JButton(new ImageIcon(b2));
	private JButton new08 = new JButton(new ImageIcon(b2));	
	private JButton new09 = new JButton(new ImageIcon(b3));
	private JButton new10 = new JButton(new ImageIcon(b3));
	private JButton new11 = new JButton(new ImageIcon(b3));
	private JButton new12 = new JButton(new ImageIcon(b3));	
	private JButton new13 = new JButton(new ImageIcon(b4));
	private JButton new14 = new JButton(new ImageIcon(b4));
	private JButton new15 = new JButton(new ImageIcon(b4));
	private JButton new16 = new JButton(new ImageIcon(b4));	
	private JButton new17 = new JButton(new ImageIcon(b5));
	private JButton new18 = new JButton(new ImageIcon(b5));
	private JButton new19 = new JButton(new ImageIcon(b5));
	private JButton new20 = new JButton(new ImageIcon(b5));	
	private JButton new21 = new JButton(new ImageIcon(b6));
	private JButton phoneButton = new JButton(new ImageIcon(phoneIcon));
	private JButton settingButton = new JButton(new ImageIcon(settingsIcon));
	private JButton new24 = new JButton(new ImageIcon(b6));	
	private JButton [] appArray = {messageButton,mailButton,timerButton,mapButton,stopWatchButton,new06,new07,
			new08,new09,new10,new11,new12,new13,new14,new15,new16,new17,new18,new19,new20,new21,phoneButton,settingButton,new24};
	
	public Phone() throws IOException {		
		mainApps.setLayout(new GridLayout(6,4));		
		for(int i=0;i<appArray.length;i++) {
			mainApps.add(appArray[i]);
			appArray[i].setMargin(new Insets(0,0,0,0));
			appArray[i].setContentAreaFilled(false);
			appArray[i].setBorder(new EmptyBorder(0,0,0,0));
			appArray[i].setFocusPainted(false);
		}		
		addButtonFunction();
		add(mainApps,BorderLayout.NORTH);
		setLayout(new GridLayout(1,1));
		setVisible(true);
		setSize(296,463);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("IOS Demo");
	}
	private void addButtonFunction() {
		messageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Messages");
				dispose();
				try {Message messageApp = new Message();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});		
		mailButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Mail");
				dispose();
				try {Mail mailApp = new Mail();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});		
		timerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Timer");
				dispose();
				try {Timers timerApp = new Timers();} 
				catch (IOException e1) {e1.printStackTrace();} 
			}});		
		stopWatchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Stop Watch");
				dispose();
				try {StopWatch stopWatchApp = new StopWatch();} 
				catch (IOException e1) {e1.printStackTrace();} 
			}});		
		mapButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Maps");
				dispose();
				try {Maps mapApp = new Maps();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});					
		phoneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Phone");
				dispose();
				try {PhoneApp phoneApp = new PhoneApp();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});		
		settingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Settings");
				dispose();
				try {Settings settingsApp = new Settings();} 
				catch (IOException e1) {e1.printStackTrace();}
			}});		
	}
}






















































