import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class PhoneApp extends JFrame{
	private JPanel displayPanel = new JPanel(),pinpadPanel = new JPanel(), enterPasscodePanel = new JPanel();
	private JButton[] numberButtons = new JButton[10];
	private JButton enterButton, clearButton, zeroButton;
	private JTextField phoneNumber = new JTextField();
	private JLabel enterPhoneNumber = new JLabel("Enter Phone Number");
	private String input = "";
	private BufferedImage one = ImageIO.read(new File("1.png"));
	private BufferedImage two = ImageIO.read(new File("2.png"));
	private BufferedImage three = ImageIO.read(new File("3.png"));
	private BufferedImage four = ImageIO.read(new File("4.png"));
	private BufferedImage five = ImageIO.read(new File("5.png"));
	private BufferedImage six = ImageIO.read(new File("6.png"));
	private BufferedImage seven = ImageIO.read(new File("7.png"));
	private BufferedImage eight = ImageIO.read(new File("8.png"));
	private BufferedImage nine = ImageIO.read(new File("9.png"));
	private BufferedImage clear = ImageIO.read(new File("clear.png"));
	private BufferedImage zero = ImageIO.read(new File("0.png"));
	private BufferedImage enter = ImageIO.read(new File("enter.png"));
	private BufferedImage[] buttonImages = { zero, one, two, three, four, five,
			six, seven, eight, nine };	
	public PhoneApp() throws IOException{		
		phoneNumber.setFont(new Font("MS San Serif", Font.BOLD, 48));
		displayPanel.setLayout(new BorderLayout());
		displayPanel.add(phoneNumber, BorderLayout.NORTH);
		pinpadPanel.setLayout(new GridLayout(4, 3));
		for (int i = 1; i <= 9; i++) {
			numberButtons[i] = new JButton(new ImageIcon(buttonImages[i]));
			numberButtons[i].setMargin(new Insets(0,0,0,0));
			numberButtons[i].setContentAreaFilled(false);
			numberButtons[i].setBorder(new EmptyBorder(0,0,0,0));
			numberButtons[i].setFocusPainted(false);
			final int x = i;
			numberButtons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					input += x;
					phoneNumber.setText(input);
				}
			});
			pinpadPanel.add(numberButtons[i]);
		}
		clearZeroEnterButtons();
		displayPanel.add(pinpadPanel, BorderLayout.CENTER);

		enterPasscodePanel.add(enterPhoneNumber);
		enterPasscodePanel.setSize(320, 20);

		add(enterPasscodePanel);
		add(displayPanel);
		setVisible(true);
		setSize(300, 470);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("IOS Demo");
	}
	private void clearZeroEnterButtons() {
		clearButton = new JButton(new ImageIcon(clear));
		clearButton.setMargin(new Insets(0,0,0,0));
		clearButton.setContentAreaFilled(false);
		clearButton.setBorder(new EmptyBorder(0,0,0,0));
		clearButton.setFocusPainted(false);
		clearButton.addActionListener(new ClearListener());		
		final int z = 0;
		zeroButton = new JButton(new ImageIcon(zero));
		zeroButton.setMargin(new Insets(0,0,0,0));
		zeroButton.setContentAreaFilled(false);
		zeroButton.setBorder(new EmptyBorder(0,0,0,0));
		zeroButton.setFocusPainted(false);
		zeroButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input+=z;
				phoneNumber.setText(input);}});		
		enterButton = new JButton(new ImageIcon(enter));
		enterButton.setMargin(new Insets(0,0,0,0));
		enterButton.setContentAreaFilled(false);
		enterButton.setBorder(new EmptyBorder(0,0,0,0));
		enterButton.setFocusPainted(false);
		enterButton.addActionListener(new EnterListener());		
		pinpadPanel.add(clearButton);
		pinpadPanel.add(zeroButton);
		pinpadPanel.add(enterButton);
	}	
	class ClearListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			phoneNumber.setText("");
			input="";
		}
	}	
	class EnterListener implements ActionListener {	
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Calling "+input);
			JOptionPane.showMessageDialog(null, input+" is not available");
			dispose();
			try {Phone menu = new Phone();} 
			catch (IOException e1) {e1.printStackTrace();}
		}
	}
}